/*
	todo:
	clean
	jshint task
	concat task
	annotate task od concat fajla
	uglify task od annotate fajla
*/
module.exports = function (grunt) {
	
	grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
		
		tag: {
            banner: "/*!\n" +
            " * <%= pkg.title %>\n" +
            " * <%= pkg.url %>\n" +
            " * @author <%= pkg.author %>\n" +
            " * @version <%= pkg.version %>\n" +
            " * Copyright (c) <%= pkg.copyright %>. All rights reserved.\n" +
            " */\n"
        },
		
		// clean task
		clean: {
			all: [
				"build/nsp-translate.js",
				"build/nsp-translate.min.js"
			],
			dev: ["build/nsp-translate.js"]
		},
		
		// code quality
		jshint: {
			all: [
				'src/app/**/*.js',
				'!src/app/TranslationObjectData_1.js'
			],
			options: {
				notypeof: true,
				debug: true,
				eqnull: true,
				eqeqeq: false,
				globals: {
					jQuery: true,
					console: true,
					module: true,
					document: true
				}
			}
		},
		
		// concat task
		concat:{
			options: {
				banner: '<%= tag.banner %>',
				separator: '\n\n/* ------------------------------------------------------------------------------------------------------------ */\n\n'
			},

			dev:{
				src: [
					'src/app/Translate.js',
					'src/app/nsp-translate.module.js',
					'src/app/service/translate.service.js',
					'src/app/directive/translate.directive.js'
				],
				dest: "build/nsp-translate.js"
			},

			production: {
				src: [
					'src/app/Translate.js',
					'src/app/nsp-translate.module.js',
					'src/app/service/translate.service.js',
					'src/app/directive/translate.directive.js'
				],
				dest: "build/nsp-translate.js"
			}
		},
		
		// uglify task
		uglify:{
			options: {
				banner: '<%= tag.banner %>',
				mangle: {
					except: ['jQuery', 'angular', '$', 'require', 'exports', 'kendo']
				}
			},
			production:{
				files: {
					'build/nsp-translate.min.js': ['<%= concat.production.dest %>']
				}
			}
		}
		
	});
	
	// load tasks
    require("jit-grunt")(grunt);

	grunt.registerTask(
		"dev", 
		[
			"clean:dev",
			"jshint:all",
			"concat:dev"
		]
	);

	grunt.registerTask(
		"production", 
		[
			"clean:all",
			"jshint:all",
			"concat:production", 
			"uglify:production"
		]
	);

};