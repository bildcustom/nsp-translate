/**
 * File name: translate.directive.js
 * Author: Uroš Gogić
 * Date: 16-Jun-16
 * Copyright (c) 2016 Bild Studio
 * http://www.bild-studio.com
 */

angular.module('nspTranslateModule').directive('nspTranslate',
    [
        "translate",
        function(translate) {

            return {
                restrict: "A",
                link: function ($scope, element, attributes) {
                    
                    var translatedValue = translate(attributes.defaultValue,attributes.nspTranslate);
                    element.html(translatedValue);
                }
            };
        }
    ]);
