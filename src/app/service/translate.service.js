angular.module('nspTranslateModule').factory('translate',
    [
        '$compile',
        function ($compile) {

            return function (text, key) {

                var translatedString;
                if (NSP.Translate.t(key)) {
                    translatedString = NSP.Translate.t(key);
                } else {
                    translatedString = isDebuggingEnabled ? key : text;
                }
                return translatedString;
            };

        }
    ]
);