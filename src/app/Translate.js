/**
 * @module NSP
 */

/**
 * NSP package
 * @namespace NSP
 */
var NSP = NSP || {};

/**
 * Translate class
 * @class Translate
 * @type {Object}
 */
NSP.Translate = NSP.Translate || {};
NSP.Translate.log = false;

/**
 * Translate function
 * @param  {string} key translate key
 * @return {string}     translated string
 */
NSP.Translate.t = function(key) {
    try{
        if (typeof NSP.Translate.TranslationObjectData !== "undefined" && NSP.Translate.TranslationObjectData !== null) {
            if (typeof key !== "undefined" && key !== null) {
                if (typeof NSP.Translate.TranslationObjectData[key] !== "undefined" && NSP.Translate.TranslationObjectData[key] !== null) {
                    return NSP.Translate.TranslationObjectData[key];
                } else {
                    if (NSP.Translate.log) {
                        throw new Error("Key '" + key + "' not found");
                    }
                }
            } else {
                if (NSP.Translate.log) {
                    throw new Error("Argument 'key' is undefined or null");
                }
            }
        } else {
            return null;
        }
    }
    catch(exception) {
        NSP.Log.error("NSP.Translate.t method: ", exception);
    }
};
