/*!
 * NSP Translate
 * www.bild-studio.net
 * @author Bild Studio
 * @version 1.0.0
 * Copyright (c) bild-studio.net. All rights reserved.
 */
/**
 * @module NSP
 */

/**
 * NSP package
 * @namespace NSP
 */
var NSP = NSP || {};

/**
 * Translate class
 * @class Translate
 * @type {Object}
 */
NSP.Translate = NSP.Translate || {};
NSP.Translate.log = false;

/**
 * Translate function
 * @param  {string} key translate key
 * @return {string}     translated string
 */
NSP.Translate.t = function(key) {
    try{
        if (typeof NSP.Translate.TranslationObjectData !== "undefined" && NSP.Translate.TranslationObjectData !== null) {
            if (typeof key !== "undefined" && key !== null) {
                if (typeof NSP.Translate.TranslationObjectData[key] !== "undefined" && NSP.Translate.TranslationObjectData[key] !== null) {
                    return NSP.Translate.TranslationObjectData[key];
                } else {
                    if (NSP.Translate.log) {
                        throw new Error("Key '" + key + "' not found");
                    }
                }
            } else {
                if (NSP.Translate.log) {
                    throw new Error("Argument 'key' is undefined or null");
                }
            }
        } else {
            return null;
        }
    }
    catch(exception) {
        NSP.Log.error("NSP.Translate.t method: ", exception);
    }
};


/* ------------------------------------------------------------------------------------------------------------ */

/**
 * File name: nsp-translate.module.js
 * Author: Uroš Gogić
 * Date: 16-Jun-16
 * Copyright (c) 2016 Bild Studio
 * http://www.bild-studio.com
 */
var nspTranslateModule = angular.module("nspTranslateModule",
    [
        
    ]
);

/* ------------------------------------------------------------------------------------------------------------ */

angular.module('nspTranslateModule').factory('translate',
    [
        '$compile',
        function ($compile) {

            return function (text, key) {

                var translatedString;
                if (NSP.Translate.t(key)) {
                    translatedString = NSP.Translate.t(key);
                } else {
                    translatedString = isDebuggingEnabled ? key : text;
                }
                return translatedString;
            };

        }
    ]
);

/* ------------------------------------------------------------------------------------------------------------ */

/**
 * File name: translate.directive.js
 * Author: Uroš Gogić
 * Date: 16-Jun-16
 * Copyright (c) 2016 Bild Studio
 * http://www.bild-studio.com
 */

angular.module('nspTranslateModule').directive('nspTranslate',
    [
        "translate",
        function(translate) {

            return {
                restrict: "A",
                link: function ($scope, element, attributes) {
                    
                    var translatedValue = translate(attributes.defaultValue,attributes.nspTranslate);
                    element.html(translatedValue);
                }
            };
        }
    ]);
